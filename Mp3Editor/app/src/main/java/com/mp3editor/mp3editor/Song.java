package com.mp3editor.mp3editor;

/**
 * Created by Zoom on 3/15/2017.
 */

import android.net.Uri;

public class Song {

    private Integer id;
    private String name;
    private String displayName;
    private String albumImage;
    private String path;
    private String duration;
    private String artist;
    private String album;
    private Integer albumId;
    private Integer genreId;

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getLyrics() {
        return lyrics;
    }

    private String lyrics;


    public String getGenre() { return genre; }

    public void setGenre(String genre) { this.genre = genre; }

    public Integer getGenreId() { return genreId; }

    public void setGenreId(Integer genreId) { this.genreId = genreId; }

    private String genre;

    public Song() { }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public void setAlbumImage(String albumImage) {
        this.albumImage = albumImage;
    }

    public String getAlbumImage() {
        return albumImage;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDisplayName() { return displayName; }

    public String getDuration() {
        return duration;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

}
