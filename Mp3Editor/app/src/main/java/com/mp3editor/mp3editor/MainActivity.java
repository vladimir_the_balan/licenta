package com.mp3editor.mp3editor;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    DrawerLayout navigation;
    ActionBarDrawerToggle navToggle;
    ActionBar actionBar;
    Toolbar mainToolbar;
    HomeFragment homeFragment;
    MyMusicFragment musicFragment;
    ArtistsFragment artistsFragment;
    private ListView menu;
    SearchView sv;
    ArrayList<NavItem> navigationItems = new ArrayList<NavItem>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        initializeNavigationItems();


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigation = (DrawerLayout)findViewById(R.id.activity_main);
        mainToolbar = (Toolbar)findViewById(R.id.main_toolbar);
        menu = (ListView)findViewById(R.id.left_drawer);
        menu.setAdapter(new NavigationAdapter(this,navigationItems));
        menu.setOnItemClickListener(new DrawerItemClickListener());


        setSupportActionBar(mainToolbar);
        navToggle = new ActionBarDrawerToggle(this,navigation,R.string.nav_open,R.string.nav_close);
        navigation.setDrawerListener(navToggle);
        navToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        homeFragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame,homeFragment,"homeFragment");
        fragmentTransaction.commit();
    }


    @Override
    protected void onStart() {
//        initializeNavigationItems();


        super.onStart();
        homeFragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame,homeFragment,"homeFragment");
        fragmentTransaction.commit();
    }

    private void initializeNavigationItems(){
        NavItem ni = new NavItem("Home",R.drawable.ic_home_black_24dp,"home");
        navigationItems.add(ni);
        ni = new NavItem("My Music",R.drawable.ic_play_circle_filled_black_24dp,"music");
        navigationItems.add(ni);
        ni = new NavItem("Artists",R.drawable.ic_supervisor_account_black_24dp,"artist");
        navigationItems.add(ni);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if(homeFragment.isVisible()){
            homeFragment.setFilter(newText);
        } else if(artistsFragment.isVisible()) {
            artistsFragment.filter(newText);
        }

        return false;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            navigation.closeDrawer(menu);
            final int pos = position;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    selectItem(pos);
                }
            }, 300);
        }
    }


    private void selectItem(int position) {
        NavItem item= navigationItems.get(position);
        String id = item.getId();
        navigation.closeDrawers();
        if (id .equals("home")) {
            HomeFragment fragment = new HomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,"homeFragment");
            fragmentTransaction.commit();
        } else if (id .equals("music")) {
            musicFragment = new MyMusicFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,musicFragment,"myMusicFragment");
            fragmentTransaction.commit();
        } else if (id .equals("artist")) {
            artistsFragment = new ArtistsFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,artistsFragment,"artistsFragment");
            fragmentTransaction.commit();
        }

    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.search);
        sv = (SearchView)item.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        sv.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        sv.setIconifiedByDefault(false);
        sv.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(navToggle.onOptionsItemSelected(item)){
            if (!sv.isIconified()) {
                sv.setIconified(true);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void main(){

    }

}
