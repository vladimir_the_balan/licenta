package com.mp3editor.mp3editor;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.GetChars;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mp3editor.mp3editor.dummy.DummyContent;
import com.mp3editor.mp3editor.dummy.DummyContent.DummyItem;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SongFragment extends Fragment {



    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SongFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public void editSongName(View view){

    }

    public Map<Integer, String> getGenrecursor(Context context) {
        Map<Integer,String> genres = new HashMap<Integer,String>();
        ContentResolver cr = context.getContentResolver();
        Uri uri = MediaStore.Audio.Genres.getContentUri("external");
        String genre = MediaStore.Audio.Genres.NAME;
        final String[] columns = { "_id", genre };
        int count = 0;
        Cursor crs = cr.query(uri, columns, null, null, null);
        if(crs != null && crs.getCount() > 0)
        {
            crs.moveToFirst();
            genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            while(crs.moveToNext()){
                genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            }
        }
        return genres;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_song, container, false);
        Song song =null;
        if(getArguments().getString("EXTRA_GENRE")!=null){
            song = getSong(getArguments().getString("EXTRA_ID"),getArguments().getString("EXTRA_GENRE"));
        } else {
            song = getSong(getArguments().getString("EXTRA_ID"),"");
        }

        Log.v("song path",song.getPath());
        TextView tv = (TextView) view.findViewById(R.id.song_name);
        if(song!=null){
            tv.setText(song.getName());
            TextView arn = (TextView) view.findViewById(R.id.artist_name);
            if(song.getArtist()!= null){
                arn.setText(song.getArtist());
            } else{
                arn.setText("-");
            }

            TextView aln = (TextView) view.findViewById(R.id.album_name);
            if(song.getAlbum()!= null){
                aln.setText(song.getAlbum());
            } else{
                aln.setText("-");
            }

            TextView gn = (TextView) view.findViewById(R.id.genre_name);
            if(song.getGenre() != null){
                gn.setText(song.getGenre());
            } else{
                gn.setText("-");
            }
            TextView d = (TextView) view.findViewById(R.id.duration_mins);
            int duration = Integer.parseInt(song.getDuration())/1000;
            d.setText(String.valueOf(duration/60)+":"+String.valueOf(duration%60));
        }
        return view;
    }

    private Song getSong(String id , String genre)
    {
        Map<Integer,String> genres = getGenrecursor(getActivity());
        ContentResolver cr = getActivity().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media._ID + "=" +id;
        Cursor cur = cr.query(uri, null, selection, null, null);
        int count = 0;
        Mp3File songFile = null;
        Song song = new Song();
        if(cur != null)
        {
            count = cur.getCount();
            if(count > 0)
            {
                cur.moveToNext();
                song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA)));
                try{
                    songFile = new Mp3File(song.getPath());
                }catch (Exception e){

                }
                if(songFile != null){
                    ID3v2 id3v2tag = songFile.getId3v2Tag();
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(id3v2tag.getTitle());
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(id3v2tag.getAlbum());
                    song.setArtist(id3v2tag.getArtist());
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genres.get(id3v2tag.getGenre()));
                } else {
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
                    song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genre);
                }

            }
        }

        return song;
    }



}
