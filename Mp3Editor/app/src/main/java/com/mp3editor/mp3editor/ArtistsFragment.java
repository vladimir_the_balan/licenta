package com.mp3editor.mp3editor;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.Arrays;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;


public class ArtistsFragment extends Fragment implements AdapterView.OnItemClickListener {

    ArrayList<String> artists;
    ArrayList<String> filteredArtists;
    ListView listView;

    public ArtistsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ArrayList<String> getArtists(){
        ContentResolver cr = getActivity().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cur = cr.query(uri, null, null, null, null);
        ArrayList<String> artists = new ArrayList<String>();
        int count = 0;
        if(cur != null)
        {
            count = cur.getCount();

            if(count > 0)
            {
                while(cur.moveToNext())
                {
                    if(!artists.contains(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)))){
                        artists.add(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    }
                }
            }
        }
        cur.close();
        return artists;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        artists = getArtists();
        Collections.sort(artists.subList(1, artists.size()));
        filteredArtists = artists;
        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.artist_row, filteredArtists);
        View view = inflater.inflate(R.layout.fragment_artists, container, false);
        listView = (ListView) view.findViewById(R.id.artists_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        return view;
    }

    public void filter(String text) {
        filteredArtists = new ArrayList<String>();
        for(String artist:artists){
            if(artist.toLowerCase().contains(text.toLowerCase())) {
                filteredArtists.add(artist);
            }
        }
        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.artist_row, filteredArtists);
        listView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.v("artist",filteredArtists.get(position));
    }
}
