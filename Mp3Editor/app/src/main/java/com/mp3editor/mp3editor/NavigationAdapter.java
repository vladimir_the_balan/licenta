package com.mp3editor.mp3editor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zoom on 3/19/2017.
 */

public class NavigationAdapter extends ArrayAdapter<NavItem> {
    public NavigationAdapter(Context context, ArrayList<NavItem> navItems) {
        super(context,R.layout.navigation_row ,navItems);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(getContext());
        View customView = li.inflate(R.layout.navigation_row , parent, false);
        NavItem item = getItem(position);
        TextView tv = (TextView)customView.findViewById(R.id.nav_text_view);
        tv.setText(item.getText());
        ImageView iv = (ImageView)customView.findViewById(R.id.nav_img_view);
        iv.setImageResource(item.getIcon());
        return customView;
    }
}
