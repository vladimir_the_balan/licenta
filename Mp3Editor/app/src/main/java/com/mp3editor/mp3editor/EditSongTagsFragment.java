package com.mp3editor.mp3editor;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v1Tag;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EditSongTagsFragment extends Fragment {

    private Song song;

    private TextView tw = null;

    public EditSongTagsFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public Map<Integer, String> getGenrecursor(Context context) {
        Map<Integer,String> genres = new HashMap<Integer,String>();
        ContentResolver cr = context.getContentResolver();
        Uri uri = MediaStore.Audio.Genres.getContentUri("external");
        String genre = MediaStore.Audio.Genres.NAME;
        final String[] columns = { "_id", genre };
        int count = 0;
        Cursor crs = cr.query(uri, columns, null, null, null);
        if(crs != null && crs.getCount() > 0)
        {
            crs.moveToFirst();
            genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            while(crs.moveToNext()){
                genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            }
        }
        return genres;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Map<Integer,String> genres = getGenrecursor(getActivity());
        EditText et = null;
        View view = inflater.inflate(R.layout.fragment_edit_song_tags, container, false);
        song = getSong(getArguments().getString("EXTRA_ID"),getArguments().getString("EXTRA_GENRE"));
        Mp3File mp3 = null;
        try {
            mp3 = new Mp3File(song.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedTagException e) {
            e.printStackTrace();
        } catch (InvalidDataException e) {
            e.printStackTrace();
        }
        if(song!=null && mp3!=null){
            ID3v2 id3v2 = mp3.getId3v2Tag();
            ID3v1 id3v1 = mp3.getId3v1Tag();
            try{
                et = (EditText) view.findViewById(R.id.title);
                et.setText(id3v2.getTitle());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Title problem",Toast.LENGTH_SHORT).show();
            }
            try{
                et = (EditText) view.findViewById(R.id.artist);
                et.setText(id3v2.getArtist());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Artist problem",Toast.LENGTH_SHORT).show();
            }
            try{
                et = (EditText) view.findViewById(R.id.album);
                et.setText(id3v2.getAlbum());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Album problem",Toast.LENGTH_SHORT).show();
            }
            try{
                et = (EditText) view.findViewById(R.id.year);
                et.setText(id3v2.getYear());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Year problem",Toast.LENGTH_SHORT).show();
            }

            try{
                et = (EditText) view.findViewById(R.id.lyrics);
                et.setText(id3v2.getLyrics());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Lyrics problem",Toast.LENGTH_SHORT).show();
            }

            TextView tv = null;
            try{
                tv = (TextView) view.findViewById(R.id.genre);
                Log.v("genre is",String.valueOf(id3v2.getGenre()));
                tv.setText(genres.get(id3v2.getGenre()));
            } catch(Exception e){
                Toast.makeText(getActivity(),"Genre problem",Toast.LENGTH_SHORT).show();
            }
            try{
                et = (EditText) view.findViewById(R.id.publisher);
                et.setText(id3v2.getPublisher());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Publisher problem!",Toast.LENGTH_SHORT).show();
            }
            try{
                et = (EditText) view.findViewById(R.id.comments);
                et.setText(id3v2.getComment());
            } catch(Exception e){
                Toast.makeText(getActivity(),"Comments problem!",Toast.LENGTH_SHORT).show();
            }
            try{
                byte[] albumImageData = id3v2.getAlbumImage();
                ImageView iv = (ImageView) view.findViewById(R.id.album_image);
                Glide.with(getActivity())
                        .load(albumImageData)
                        .into(iv);
            } catch(Exception e){
                Toast.makeText(getActivity(),"Comments problem!",Toast.LENGTH_SHORT).show();
            }

        }
        EditText EtOne = (EditText) view.findViewById(R.id.lyrics);
        EtOne.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.lyrics) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;
    }




    private Song getSong(String id , String genre)
    {
        ContentResolver cr = getActivity().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media._ID + "=" +id;
        Cursor cur = cr.query(uri, null, selection, null, null);
        int count = 0;
        Mp3File songFile = null;
        Map<Integer,String> genres = getGenrecursor(getActivity());
        Song song = new Song();
        if(cur != null)
        {
            count = cur.getCount();
            if(count > 0)
            {
                cur.moveToNext();
                song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA)));
                try{
                    songFile = new Mp3File(song.getPath());
                }catch (Exception e){

                }
                if(songFile != null){
                    ID3v2 id3v2tag = songFile.getId3v2Tag();
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(id3v2tag.getTitle());
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(id3v2tag.getAlbum());
                    song.setArtist(id3v2tag.getArtist());
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genres.get(id3v2tag.getGenre()));
                } else {
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
                    song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genre);
                }
            }
        }

        return song;
    }

}
