package com.mp3editor.mp3editor;

/**
 * Created by Zoom on 3/19/2017.
 */

public class NavItem {

    private String text = "";
    private int icon ;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NavItem(){

    }

    public NavItem(String text , int icon, String id){
        this.text = text;
        this.icon = icon;
        this.id = id;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
