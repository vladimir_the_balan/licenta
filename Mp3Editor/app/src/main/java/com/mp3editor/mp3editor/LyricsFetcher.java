package com.mp3editor.mp3editor;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.lang3.StringEscapeUtils;

public class LyricsFetcher implements Response.ErrorListener, Response.Listener<String> {

    private boolean xml = true;
    private String url = "";
    private Context context;
    private String title;
    private String artist;
    private EditSongTagsFragment fragment;

    private String fixupInput(String input) {
        String result = input.replaceAll(" ", "_");
        return result;
    }

    private String getUrlFromXml(String content) {
        String result = "";
        String[] parts = content.split("<url>");
        parts = parts[1].split("</url>");
        result = parts[0];
        return result;
    }

    private String extractLyricsFromPage(String content) {
        StringBuilder result = new StringBuilder();
        String[] parts = content.split("<div class='lyricbox'>");
        if (parts.length < 2) {
            return null;
        }
        parts = parts[1].split("<div");
        return StringEscapeUtils.unescapeHtml4(parts[0]).replace("<br />","\n");
    }



    public void fetchLyrics(String artist, String title, Context context,EditSongTagsFragment fragment) {
        // need to call out and get url  http://lyrics.wikia.com/api.php?func=getSong&artist=Tool&song=Schism&fmt=xml
        this.artist = artist;
        this.title = title;
        this.context = context;
        this.fragment = fragment;
        url = "http://lyrics.wikia.com/api.php?func=getSong&artist=" + fixupInput(artist) + "&song=" + fixupInput(title) + "&fmt=xml";
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,this,this);
        queue.add(stringRequest);

    }

    public void fetchLyrics(String artist, String title){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,this,this);
        queue.add(stringRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        if(xml){
            url = this.getUrlFromXml(response);
            xml = false;
            this.fetchLyrics(this.artist,this.title);
        } else {
            String lyrics = this.extractLyricsFromPage(response);
            View view = fragment.getView();
            EditText et = (EditText)view.findViewById(R.id.lyrics);
            et.setText(lyrics);
            Toast.makeText(context,"Got lyrics from http://lyrics.wikia.com", Toast.LENGTH_LONG).show();
        }
    }
}