package com.mp3editor.mp3editor;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Zoom on 6/23/2017.
 */
public class ImageFetcher implements Response.ErrorListener, Response.Listener<String> {

    private boolean xml = true;
    private String url = "";
    private Context context;
    private String album;
    private String artist;
    private String year = "";
    private EditSongTagsFragment fragment;
    ArrayList<String> genres ;

    private String fixupInput(String input) {
        String result = input.replaceAll(" ", "_");
        return result;
    }

    private String getUrlFromXml(String content) {
        String result = "";
        String[] parts = content.split("<url>");
        parts = parts[1].split("</url>");
        result = parts[0];
        return result;
    }

    private String extractImageFromPage(String content) {
        Document doc = Jsoup.parse(content);
        Elements images = doc.getElementsByTag("a");
        genres = new ArrayList<>();
        for(Element image:images){
            if(image.attr("href").contains("/wiki/Category:Genre")){
                genres.add(image.text());
            }
        }
        for(Element image:images){
            if(image.attr("title").toLowerCase().trim().equals(album.toLowerCase().trim()) && image.attr("class").contains("image image-thumbnail")){
                Log.v("my-image",image.attr("href"));
                return image.attr("href");
            }
        }
        return "";
    }



    public void fetchImage( Context context,EditSongTagsFragment fragment) {
        // need to call out and get url  http://lyrics.wikia.com/api.php?func=getSong&artist=Tool&song=Schism&fmt=xml
        EditText et = (EditText) fragment.getView().findViewById(R.id.artist);
        this.artist = et.getText().toString();
        et = (EditText) fragment.getView().findViewById(R.id.album);
        this.album = et.getText().toString();
        this.context = context;
        this.fragment = fragment;
        url = "http://lyrics.wikia.com/wiki/" + fixupInput(artist);
        Log.v("url",url);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,this,this);
        queue.add(stringRequest);

    }
    public ArrayList<String> getGenrecursor(Context context) {
        ArrayList<String> genres = new ArrayList<String>();
        ContentResolver cr = context.getContentResolver();
        Uri uri = MediaStore.Audio.Genres.getContentUri("external");
        String genre = MediaStore.Audio.Genres.NAME;
        final String[] columns = { "_id", genre };
        int count = 0;
        Cursor crs = cr.query(uri, columns, null, null, null);
        if(crs != null && crs.getCount() > 0)
        {
            crs.moveToFirst();
            genres.add(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            while(crs.moveToNext()){
                genres.add(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            }
        }
        return genres;
    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        if(album.length() > 0){
            String link = this.extractImageFromPage(response);
            ImageView iv = (ImageView) fragment.getView().findViewById(R.id.album_image);
            if(link.length() > 0){
                Glide.with(context).load(link).override(1300,1300).into(iv);
            }
            if(genres.size() > 0){
                ArrayList<String> existentGenres = getGenrecursor(context);
                for(String foundGenre : genres) {
                    for(String existentGenre : existentGenres) {
                        if(foundGenre.equals(existentGenre)){
                            TextView tv = (TextView) fragment.getView().findViewById(R.id.genre);
                            tv.setText(foundGenre);
                            return;
                        }
                    }
                }
            }
        }
//
    }
}

