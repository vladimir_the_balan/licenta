package com.mp3editor.mp3editor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;

/**
 * Created by Zoom on 3/19/2017.
 */

public class MusicListRecyclerAdapter extends RecyclerView.Adapter<MusicListRecyclerAdapter.MusicHolder> {

    private List<Song> songs;

    private LayoutInflater inflater;

    private ItemClickCallback itemClickCallback;

    public interface ItemClickCallback {
        void onItemClick(int p);
    }

    public void setItemClickCallback (final ItemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    private static int height;

    public MusicListRecyclerAdapter(List<Song> inputSongs , Context c){
        this.inflater = LayoutInflater.from(c);
        this.songs = inputSongs;
    }

    @Override
    public MusicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.song_row, parent, false);
        return new MusicHolder(view);
    }

    @Override
    public void onBindViewHolder(MusicHolder holder, int position) {
        Song song = songs.get(position);
        holder.songName.setText(song.getDisplayName());
        holder.songArtist.setText(song.getArtist());

        Glide.with(holder.songImage.getContext())
                .load(song.getAlbumImage())
                .centerCrop()
                .placeholder(R.drawable.mus_note)
                .into(holder.songImage);
    }


    @Override
    public int getItemCount() {
        return songs.size();
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth() ,height = bitmap.getHeight() ;
        return Bitmap.createBitmap(bitmap, (bitmap.getWidth()-bitmap.getHeight())/2,0,bitmap.getHeight(),bitmap.getHeight());
    }

    class MusicHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView songName;
        private ImageView songImage;
        private TextView songArtist;
        private View row;

        public MusicHolder(View itemView) {
            super(itemView);
            songName = (TextView)itemView.findViewById(R.id.song_name);
            songArtist = (TextView) itemView.findViewById(R.id.song_artist);
            songImage = (ImageView)itemView.findViewById(R.id.song_image);
            row = itemView.findViewById(R.id.song_row);
            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.song_row){
                itemClickCallback.onItemClick(getAdapterPosition());
            }
        }
    }



}
