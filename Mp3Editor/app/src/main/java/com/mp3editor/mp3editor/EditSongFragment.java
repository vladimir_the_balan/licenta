package com.mp3editor.mp3editor;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;

import java.util.HashMap;
import java.util.Map;


public class EditSongFragment extends Fragment {

    private Song song;
    private EditText et;

    public EditSongFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_song, container, false);
        song = getSong(getArguments().getString("EXTRA_ID"),getArguments().getString("EXTRA_GENRE"));
        if(song!=null){
            et = (EditText) view.findViewById(R.id.change_name);
            et.setText(song.getDisplayName());
        }
        return view;
    }

    public Map<Integer, String> getGenrecursor(Context context) {
        Map<Integer,String> genres = new HashMap<Integer,String>();
        ContentResolver cr = context.getContentResolver();
        Uri uri = MediaStore.Audio.Genres.getContentUri("external");
        String genre = MediaStore.Audio.Genres.NAME;
        final String[] columns = { "_id", genre };
        int count = 0;
        Cursor crs = cr.query(uri, columns, null, null, null);
        if(crs != null && crs.getCount() > 0)
        {
            crs.moveToFirst();
            genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            while(crs.moveToNext()){
                genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            }
        }
        return genres;
    }
    private Song getSong(String id , String genre)
    {
        ContentResolver cr = getActivity().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media._ID + "=" +id;
        Cursor cur = cr.query(uri, null, selection, null, null);
        int count = 0;
        Song song = new Song();
        Mp3File songFile = null;
        Map<Integer,String> genres = getGenrecursor(getActivity());
        if(cur != null)
        {
            count = cur.getCount();
            if(count > 0)
            {
                cur.moveToNext();
                song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA)));
                try{
                    songFile = new Mp3File(song.getPath());
                }catch (Exception e){

                }
                if(songFile != null){
                    ID3v2 id3v2tag = songFile.getId3v2Tag();
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(id3v2tag.getTitle());
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(id3v2tag.getAlbum());
                    song.setArtist(id3v2tag.getArtist());
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genres.get(id3v2tag.getGenre()));
                } else {
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
                    song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genre);
                }

            }
        }

        return song;
    }

    public void addAlbum(){
        if(song.getAlbum() != null){
            et.setText(et.getText()+"-"+song.getAlbum());
        } else {
            Toast.makeText(getActivity(),"There is no album",Toast.LENGTH_SHORT).show();
        }

    }
    public void addGenre(){

        if(song.getGenre() != null){
            et.setText(et.getText()+"-"+song.getGenre());
        } else {
            Toast.makeText(getActivity(),"There is no genre",Toast.LENGTH_SHORT).show();
        }
    }
    public void addArtist(){

        if(song.getArtist() != null){
            et.setText(et.getText()+"-"+song.getArtist());
        } else {
            Toast.makeText(getActivity(),"There is no artist",Toast.LENGTH_SHORT).show();
        }
    }
    public void addTitle(){

        if(song.getName() != null){
            et.setText(et.getText()+"-"+song.getName());
        } else {
            Toast.makeText(getActivity(),"There is no album",Toast.LENGTH_SHORT).show();
        }
    }


}
