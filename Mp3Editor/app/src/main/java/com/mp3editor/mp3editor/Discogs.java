package com.mp3editor.mp3editor;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Zoom on 6/22/2017.
 */

public class Discogs implements Response.ErrorListener, Response.Listener<String>{

    private String artist;
    private String title;
    private Context context;
    private EditSongTagsFragment fragment;
    private Database db;
    private String consumerKey = "bEaNEpvjPvvhUzYoDRBJ";
    private String consumerSecret = "yRxVUXDVyCwQdChXvftUYjGjGkTcLDhv";
    private String requestTokenUrl = "https://accounts.spotify.com/authorize";
    private String authorization = "OAuth oauth_consumer_key=\""+ consumerKey +"\", " +
            " oauth_nonce=\"nou\", " +
            " oauth_signature=\""+ consumerSecret +"&\", " +
            " oauth_signature_method=\"PLAINTEXT\", " +
            " oauth_timestamp=\"current_timestamp\", " +
            " oauth_callback=\"your_callback\"";



    public void getOtherData(String artist, String title, Context context, EditSongTagsFragment fragment){
        this.artist = artist;
        this.title = title;
        this.context = context;
        this.fragment = fragment;
        db = new Database(context,null,null,1);
        Log.v("auth",authorization);
        this.getRequestToken();
        if(db.getOauthToken() != null && db.getOauthToken() != null){
            this.requestDataFromServer();
        } else {

        }
        Toast.makeText(context,db.getOauthToken() + db.getOauthTokenSecret(), Toast.LENGTH_LONG).show();
    }

    private void requestDataFromServer() {

    }

    public void getRequestToken(){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Request.Method.GET, requestTokenUrl,this,this);
//        {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String>  params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                params.put("Authorization", authorization);
//                params.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36 " + "Chrome 41.0.2227.1");
//                return params;
//            }
//        };
        queue.add(postRequest);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Log.v("request","crashed");
    }

    @Override
    public void onResponse(String response) {
        Log.v("request",response);
    }
}
