package com.mp3editor.mp3editor;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HomeFragment extends Fragment implements MusicListRecyclerAdapter.ItemClickCallback  {


    private ArrayList<Song> songs;
    private ArrayList<Song> filteredSongs;
    private RecyclerView rv;
    MusicListRecyclerAdapter adapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    private ArrayList<Song> getSongs(){
        ContentResolver cr = getActivity().getContentResolver();
        Map<Integer, String> myAlbums = new HashMap<Integer, String>();
        Map<Integer, String> myGenres = new HashMap<Integer, String>();
        Map<Integer, Integer> mySongGenre = new HashMap<Integer, Integer>();
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String selection ;
        String sortOrder ;
        Cursor cur = cr.query(uri, null, null, null, null);
        int count = 0;
        int i = 0;
        String tableString = "";
        if(cur != null)
        {
            count = cur.getCount();
            if(count > 0)
            {
                while(cur.moveToNext())
                {
                    myAlbums.put(Integer.parseInt(cur.getString(cur.getColumnIndex("_id"))),cur.getString(cur.getColumnIndex("album_art")));
                }
            }
        }


        uri = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
        cur = cr.query(uri, null, null, null, null);
        count = 0;
        tableString = "";
        if(cur != null)
        {
            count = cur.getCount();
            if(count > 0)
            {
                while(cur.moveToNext())
                {
                    myGenres.put(Integer.parseInt(cur.getString(cur.getColumnIndex("_id"))),cur.getString(cur.getColumnIndex("name")));
                    Uri inneruri =  MediaStore.Audio.Genres.Members.getContentUri("external", Integer.parseInt(cur.getString(cur.getColumnIndex("_id"))));
                    int innercount = 0;
                    Cursor innercur = cr.query(inneruri, null, null, null, null);
                    if(innercur != null)
                    {
                        innercount = cur.getCount();
                        if(innercount > 0)
                        {
                            while(innercur.moveToNext())
                            {
                                mySongGenre.put(Integer.parseInt(innercur.getString(innercur.getColumnIndex("_id"))),Integer.parseInt(cur.getString(cur.getColumnIndex("_id"))));
                            }
                        }
                    }
                }
            }
        }


        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        cur = cr.query(uri, null, selection, null, sortOrder);
        count = 0;
        Log.v("genre",String.valueOf(mySongGenre.get(5119)));
        ArrayList<Song> songs = new ArrayList<Song>();
        int songId = -1;
        if(cur != null)
        {
            count = cur.getCount();

            if(count > 0)
            {
                while(cur.moveToNext())
                {
                    Song song = new Song();
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setAlbum(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
                    song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));
                    song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA)));
                    if(myAlbums.get(song.getAlbumId())!= null){
                        song.setAlbumImage(myAlbums.get(song.getAlbumId()));
                    }
                    songId = cur.getInt(cur.getColumnIndex(MediaStore.Audio.Media._ID));
                    song.setGenre(myGenres.get(mySongGenre.get(songId)));
                    song.setGenreId(mySongGenre.get(songId));
                    songs.add(song);
                }
            }
        }
        cur.close();
        return songs;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        songs = getSongs();
        filteredSongs = getSongs();
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.musicListView);
        rv.setLayoutManager(mLinearLayoutManager);
        adapter = new MusicListRecyclerAdapter(songs ,getActivity());
        rv.setAdapter(adapter);
        adapter.setItemClickCallback(this);
        return rootView;
    }

    @Override
    public void onItemClick(int p) {
        Song song = (Song) filteredSongs.get(p);
        Intent intent = new Intent(getActivity(),EditSong.class);
        Bundle extra = new Bundle();
        extra.putString("EXTRA_ID",String.valueOf(song.getId()));
        extra.putString("EXTRA_GENRE",String.valueOf(song.getGenreId()));
        intent.putExtra("EXTRA",extra);
        startActivity(intent);
    }

    public void setFilter(String text){

        filteredSongs = new ArrayList<>();
        for(Song song:songs){
            if(song.getName().toLowerCase().contains(text.toLowerCase()) || song.getArtist().toLowerCase().contains(text.toLowerCase())){
                filteredSongs.add(song);
            }
        }
        adapter = new MusicListRecyclerAdapter(filteredSongs ,getActivity());
        rv.setAdapter(adapter);
        adapter.setItemClickCallback(this);
    }
}
