package com.mp3editor.mp3editor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by Zoom on 6/22/2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "mp3editor";
    private static final String TABLE_OAUTH = "oauth_creds";
    private static final String OAUTH_TOKEN = "oauth_token";
    private static final String OAUTH_TOKEN_SECRET = "oauth_token_secret";
    private static final String ID = "id";

    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE "+ TABLE_OAUTH +"( "+
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                OAUTH_TOKEN + " TEXT ," +
                OAUTH_TOKEN_SECRET + " TEXT " + ");";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public String getOauthToken(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_OAUTH,null);
        String token = "";
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            token = cursor.getString(cursor.getColumnIndex(OAUTH_TOKEN));
        }
        cursor.close();
        db.close();
        return token;
    }
    public String getOauthTokenSecret(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_OAUTH,null);
        String token = "";
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            token = cursor.getString(cursor.getColumnIndex(OAUTH_TOKEN_SECRET));
        }
        cursor.close();
        db.close();
        return token;
    }

    public void modifyToken(String token){
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_OAUTH+" WHERE "+ID+"=1",null);
        if(cursor.getCount() > 0) {
            db.execSQL("UPDATE "+TABLE_OAUTH+" SET "+OAUTH_TOKEN+"=\""+token+"\"  WHERE "+ID+"=1;");
        } else {
            ContentValues values = new ContentValues();
            values.put(OAUTH_TOKEN,token);
            Log.v("status",String.valueOf(db.insert(TABLE_OAUTH,null,values)));
        }
        cursor.close();
        db.close();
    }


    public void modifyTokenSecret(String token){
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_OAUTH+" WHERE "+ID+"=1",null);
        if(cursor.getCount() > 0) {
            db.execSQL("UPDATE "+TABLE_OAUTH+" SET "+OAUTH_TOKEN_SECRET+"=\""+token+"\"  WHERE "+ID+"=1;");
        } else {
            ContentValues values = new ContentValues();
            values.put(OAUTH_TOKEN_SECRET,token);
            Log.v("status",String.valueOf(db.insert(TABLE_OAUTH,null,values)));
        }
        cursor.close();
        db.close();
    }

    public String getTableAsString() {
        SQLiteDatabase db = getWritableDatabase();
        String tableName = TABLE_OAUTH;
        Log.d(TAG, "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = db.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }

}
