package com.mp3editor.mp3editor;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.NotSupportedException;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditSong extends AppCompatActivity {

    private EditSongFragment editFragment;
    private EditSongTagsFragment editTagsFragment;
    private SongFragment songFragment;
    private Bundle extras;
    private static final int PICK_PICTURE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_song);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("EDIT SONG");
        extras = getIntent().getBundleExtra("EXTRA");
        songFragment = new SongFragment();
        songFragment.setArguments(extras);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.song_frame,songFragment,"songFragment");
        fragmentTransaction.commit();
    }

    public void addTitle(View view){
        editFragment.addTitle();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void addArtist(View view){
        editFragment.addArtist();
    }

    public void addAlbum(View view){
        editFragment.addAlbum();
    }

    public void addGenre(View view){
        editFragment.addGenre();
    }

    public void cancel(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        songFragment.setArguments(extras);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.song_frame,songFragment,"songFragment");
        fragmentTransaction.commit();
    }
    public void cancelTags(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        songFragment.setArguments(extras);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.song_frame,songFragment,"songFragment");
        fragmentTransaction.commit();
    }

    public void onBackPressed()
    {
        Log.v("back pressed","yep");
        if(editFragment != null && editFragment.isVisible()){
            songFragment.setArguments(extras);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.song_frame,songFragment,"songFragment");
            fragmentTransaction.commit();
        } else if (editTagsFragment != null && editTagsFragment.isVisible()){
            songFragment.setArguments(extras);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.song_frame,songFragment,"songFragment");
            fragmentTransaction.commit();
        } else {
            finish();
        }
    }

    public void save(View view){
        Song song = getSong(extras.getString("EXTRA_ID"),extras.getString("EXTRA_GENRE"));
        EditText et  = (EditText)findViewById(R.id.change_name);
        String new_name = et.getText().toString();
        if(!new_name.equals(song.getDisplayName())){
            try {
                rename(song.getPath(),new_name);
            } catch (Exception e){
                Toast.makeText(this,"Failed renaming",Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this,"same name",Toast.LENGTH_SHORT).show();
        }
        this.cancel(view);
    }

    public void saveTags(View view){
        Song song = getSong(extras.getString("EXTRA_ID"),extras.getString("EXTRA_GENRE"));
        EditText et  = null;

        try{
            Mp3File mp3 = null;
            try {
                mp3 = new Mp3File(song.getPath());
            } catch (Exception e){
                Log.v("rename","No file!");
            }
            if (mp3.hasId3v2Tag()) {
                ID3v2 id3v2Tag = mp3.getId3v2Tag();
                et  = (EditText)findViewById(R.id.title);
                id3v2Tag.setTitle(et.getText().toString());
                et  = (EditText)findViewById(R.id.artist);
                id3v2Tag.setArtist(et.getText().toString());
                et  = (EditText)findViewById(R.id.album);
                id3v2Tag.setAlbum(et.getText().toString());
                et  = (EditText)findViewById(R.id.lyrics);
                id3v2Tag.setLyrics(et.getText().toString());
                et  = (EditText)findViewById(R.id.year);
                id3v2Tag.setYear(et.getText().toString());
                TextView tv = (TextView) findViewById(R.id.genre);
                Map<Integer,String> genres = getGenrecursor(this);
                for(Map.Entry<Integer,String>entry : genres.entrySet() ){
                    if(entry.getValue().equals(tv.getText().toString())){
                        id3v2Tag.setGenre(entry.getKey());
                    }
                }
                et  = (EditText)findViewById(R.id.publisher);
                id3v2Tag.setPublisher(et.getText().toString());
                et  = (EditText)findViewById(R.id.comments);
                id3v2Tag.setComment(et.getText().toString());
                ImageView iv = (ImageView) findViewById(R.id.album_image);
                ((GlideBitmapDrawable)iv.getDrawable().getCurrent()).getBitmap();
                Bitmap bitmap = ((GlideBitmapDrawable)iv.getDrawable().getCurrent()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageInByte = baos.toByteArray();
                id3v2Tag.setAlbumImage(imageInByte,"Mp3Editor");

                try {
                    String arg=Environment.getExternalStorageState()+"/sample.mp3";
                    mp3.save("/sdcard/sample.mp3");

                } catch (NotSupportedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                //return null;
                File fp=new File(song.getPath());
                fp.delete();
                FileInputStream inStream = new FileInputStream("/sdcard/sample.mp3");
                FileOutputStream outStream = new FileOutputStream(song.getPath());
                FileChannel inChannel = inStream.getChannel();
                FileChannel outChannel = outStream.getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);
                inStream.close();
                outStream.close();

                new SingleMediaScanner(this,new File(song.getPath()));

                File ff=new File("/sdcard/sample.mp3");
                ff.delete();
            }
            Toast.makeText(this,"Song tags changed!",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Log.v("exception",e.toString());
            Toast.makeText(this,e.toString(),Toast.LENGTH_LONG).show();
        };
        this.cancelTags(view);
    }

    private boolean rename(String from, String to) throws IOException{
        File base = new File(from);
        to = base.getParentFile().getPath()+"/"+to;
        if(base.exists()){
            File dest = new File(to);
            if(dest.exists()){
                Toast.makeText(this,"Destination exists.",Toast.LENGTH_SHORT).show();
                return false;
            } else {
                Toast.makeText(this,"File renamed!",Toast.LENGTH_SHORT).show();
                base.renameTo(dest);
                Log.v("file new name",base.getName());
                new SingleMediaScanner(this,new File(from));
                new SingleMediaScanner(this,new File(to));
                return true;
            }
        } else {
            Toast.makeText(this,"Base file does not exist.",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public Map<Integer, String> getGenrecursor(Context context) {
        Map<Integer,String> genres = new HashMap<Integer,String>();
        ContentResolver cr = context.getContentResolver();
        Uri uri = MediaStore.Audio.Genres.getContentUri("external");
        String genre = MediaStore.Audio.Genres.NAME;
        final String[] columns = { "_id", genre };
        int count = 0;
        Cursor crs = cr.query(uri, columns, null, null, null);
        if(crs != null && crs.getCount() > 0)
        {
            crs.moveToFirst();
            genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            while(crs.moveToNext()){
                genres.put(Integer.valueOf(crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres._ID))),crs.getString(crs.getColumnIndex(MediaStore.Audio.Genres.NAME)));
            }
        }
        return genres;
    }

    public void chooseGenre(View view){
        List<String> genrText = new ArrayList<String>();
        Map<Integer,String> genres = getGenrecursor(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        for(Map.Entry<Integer,String >entry:genres.entrySet()){
            genrText.add(entry.getValue());
        }
        final CharSequence[] genresText = genrText.toArray(new CharSequence[genrText.size()]);
        builder.setTitle("Choose genre");
        builder.setItems(genresText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                View view = editTagsFragment.getView();
                TextView tv = (TextView) view.findViewById(R.id.genre);
                tv.setText(genresText[which]);
            }
        });
        builder.show();
    }

    public void editSongName(View view){
        Bundle extras = getIntent().getBundleExtra("EXTRA");
        editFragment = new EditSongFragment();
        editFragment.setArguments(extras);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.song_frame,editFragment,"editSongFragment");
        fragmentTransaction.commit();
    }

    public void editSongTags(View view){
        Bundle extras = getIntent().getBundleExtra("EXTRA");
        editTagsFragment = new EditSongTagsFragment();
        editTagsFragment.setArguments(extras);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.song_frame,editTagsFragment,"editSongTagsFragment");
        fragmentTransaction.commit();
    }

    private Song getSong(String id , String genre)
    {
        ContentResolver cr = this.getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media._ID + "=" +id;
        Cursor cur = cr.query(uri, null, selection, null, null);
        int count = 0;
        Mp3File songFile = null;
        Map<Integer,String> genres = getGenrecursor(this);
        Song song = new Song();
        if(cur != null)
        {
            count = cur.getCount();
            if(count > 0)
            {
                cur.moveToNext();
                song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA)));
                try{
                    songFile = new Mp3File(song.getPath());
                }catch (Exception e){

                }
                if(songFile != null){
                    ID3v2 id3v2tag = songFile.getId3v2Tag();
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(id3v2tag.getTitle());
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(id3v2tag.getAlbum());
                    song.setArtist(id3v2tag.getArtist());
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genres.get(id3v2tag.getGenre()));
                } else {
                    song.setDisplayName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
                    song.setId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media._ID))));
                    song.setName(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    song.setDuration(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DURATION)));

                    song.setAlbum(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
                    song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    song.setAlbumId(Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));

                    song.setGenre(genre);
                }
            }
        }

        return song;
    }

    public void fill(View view){
        EditText t = (EditText) findViewById(R.id.title);
        EditText a = (EditText) findViewById(R.id.artist);
        Api api = new Api();
        if(t.length() > 0 && a.length() > 0){
            api.getSongMeta(editTagsFragment,t.getText().toString(),a.getText().toString(),this);
        } else {
            Toast.makeText(this,"Not enough information available!",Toast.LENGTH_SHORT).show();
        }

    }

    public void audioPlayer(View view){
        Song song = getSong(extras.getString("EXTRA_ID"),extras.getString("EXTRA_GENRE"));
        MediaPlayer mp = new MediaPlayer();
        try {
            mp.setDataSource(song.getPath());
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void chooseImage(View view) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
        startActivityForResult(chooserIntent, PICK_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_PICTURE && resultCode == RESULT_OK){
            Uri uri = data.getData();
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri,projection,null,null,null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();
            Log.v("file_path",filePath);
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageInByte = baos.toByteArray();
            ImageView iv = (ImageView) editTagsFragment.getView().findViewById(R.id.album_image);
            Glide.with(this).load(imageInByte).override(1300,1300).into(iv);

        }
    }
}
