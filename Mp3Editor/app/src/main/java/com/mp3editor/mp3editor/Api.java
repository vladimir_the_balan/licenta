package com.mp3editor.mp3editor;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Zoom on 6/21/2017.
 */

public class Api implements Response.Listener<String>,Response.ErrorListener{

    private static String last_api_key = "ebdacebae2d3754522253b6b979f8a0c";
    private static String last_api_url = "http://ws.audioscrobbler.com/2.0/?";

    private Context context = null;
    private View view;
    private Song song = null;
    private EditSongTagsFragment fragment;

    public void getSongMeta(EditSongTagsFragment fragment,String songName, String songArtist, Context context) {
        this.fragment = fragment;
        Map<String,String> metadata = new HashMap<String, String>();
        this.context = context;
        song = new Song();
        if (Api.isNetworkAvailable(context)) {
            this.getTrackJsonFromLastFm(songName ,songArtist ,context);
        } else {
            Toast.makeText(context,"No internet connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void getTrackJsonFromLastFm(String track, String artist, Context context) {

        artist = artist.replace(" ","%20");
        track = track.replace(" ","%20");
        String url = last_api_url + "method=track.getInfo&api_key="+last_api_key+"&artist="+artist+"&track="+track+"&autocorrect=1&format=json";
        Log.v("url",url);
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,this,this);
        queue.add(stringRequest);

    }

    private static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onResponse(String response) {
        Log.v("success",response);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            if(jsonObject.get("message").toString() != null){
                Toast.makeText(context,jsonObject.get("message").toString(),Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(jsonObject != null){

            try {
                JSONObject track = new JSONObject(jsonObject.get("track").toString());
                song.setName(track.get("name").toString());
                try {
                    JSONObject artist = new JSONObject(track.get("artist").toString());
                    song.setArtist(artist.get("name").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject album = new JSONObject(track.get("album").toString());
                    song.setAlbum(album.get("title").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                String lyric = jsonObject.get("lyric").toString();
                song.setLyrics(lyric);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            View view = fragment.getView();
            if(song != null) {
                String status = "";
                if(song.getName() != null) {
                    EditText et = (EditText) view.findViewById(R.id.title);
                    et.setText(song.getName());
                    status += "name ";
                }

                if(song.getArtist() != null) {
                    EditText et = (EditText) view.findViewById(R.id.artist);
                    et.setText(song.getArtist());
                    status += "artist ";
                }

                if(song.getAlbum() != null) {
                    EditText et = (EditText) view.findViewById(R.id.album);
                    et.setText(song.getAlbum());
                    status += "album ";
                }
                if (status != "") {
                    Toast.makeText(context,"Added "+status+"from Last FM!",Toast.LENGTH_LONG).show();
                }
                if(song.getName() != null && song.getArtist() != null){
                    DataFetcher lf = new DataFetcher();
                    lf.fetchLyrics(song.getArtist(),song.getName(),context,fragment);
                    ImageFetcher imf = new ImageFetcher();
                    imf.fetchImage( context, fragment);

//                    Discogs dgs = new Discogs();
//                    dgs.getOtherData(song.getArtist(),song.getName(),context,fragment);
                }
            } else {
                Toast.makeText(context,"No data found!",Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(context,"Error finding data ...",Toast.LENGTH_SHORT).show();
    }
}
